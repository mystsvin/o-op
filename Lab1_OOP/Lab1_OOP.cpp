// Lab1_OOP.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include "program.h"

using namespace std;
using namespace all_shapes;

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc != 3)
	{
		cout << "incorrect command line! "
			"Waited: command infile outfile" << endl;
		exit(1);
	}
	ifstream ifst(argv[1]);
	ofstream ofst(argv[2]);

	cout << "Start" << endl;
	crisper new_cris;

	new_cris.cont_add(ifst);
	ofst << "Filled container." << endl;
	new_cris.cont_out(ofst);

	new_cris.cont_sort();
	ofst << "Sorted container." << endl;
	new_cris.cont_out(ofst);

	ofst << "Multimethod." << endl;
	new_cris.cont_multi_method(ofst);

	ofst << "Only rectangels." << endl;
	new_cris.cont_rect_out(ofst);

	new_cris.cont_clear();
	ofst << "Empty container." << endl;
	new_cris.cont_out(ofst);

	cout << "Stop" << endl;
	return 0;
}

