#include "stdafx.h"
#include "CppUnitTest.h"
#include "program.h"
#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace all_shapes;
using namespace std;

namespace perimeter_Test
{
	TEST_CLASS(perimeterTests)
	{
	public:

		TEST_METHOD(perimeter_Rectangle)
		{
			shape *new_rectangle = new rectangle(1, 2, 3, 4, "Blue", 5);
			float perim = new_rectangle->shape_perim();
			float expected = 8.000000;
			Assert::AreEqual(expected, perim);
		};

		TEST_METHOD(perimeter_Circle)
		{
			shape *new_circle = new circle(1, 2, 3, "Red", 8);
			float perim = new_circle->shape_perim();
			float expected = 2 * M_PI * 1;
			Assert::AreEqual(expected, perim);
		}
		TEST_METHOD(perimeter_Triangle)
		{
			shape *new_triangle = new triangle(1, 2, 3, "Yellow", 8);
			float perim = new_triangle->shape_perim();
			float expected = 6.000000;
			Assert::AreEqual(expected, perim);
		}
	};
}