#ifndef __program_atd__
#define __program_atd__
#include "string"
#include "iostream"
#include <fstream>

using namespace std;

namespace all_shapes
{
	// ���������, ���������� ��� ������
	class shape
	{
	public:
		static shape* shape_in(ifstream &ifst); // �������������, ���������� � ���� ������ �� ������
		virtual void shape_add_data(ifstream &ifst) = 0; // ����
		virtual void shape_out_data(ofstream &ofst) = 0; // �����
		virtual void rectangle_out_data(ofstream &ofst); // ����� ������ ������ � ���������������
		virtual string shape_string_out() = 0; // ����� � ������
		virtual float shape_perim() = 0; // ���������� ���������
		float shape_perim_out(); // ����� ���������
	protected:
		shape(string t5, float t6) : color(t5), density(t6) {}; // ������������� ��������� � ������� ������������
		string color; // ���� ������
		float density; // ��������� ���������
		float perimeter; // �������� ������
	};

	// ���������, ������������ �������������
	class rectangle : public shape
	{
		int x1, y1; // ����������, ������������ ������� ����� ����
		int x2, y2; // ����������, ������������ ������ ������ ����
	public:
		rectangle(int t1, int t2, int t3, int t4, string t5, float t6) : x1(t1), y1(t2), x2(t3), y2(t4), shape(t5, t6) {}; // ������������� ��������� � ������� ������������
		~rectangle(); // ����������
		void shape_add_data(ifstream &ifst); // ����
		void shape_out_data(ofstream &ofst); // �����
		void rectangle_out_data(ofstream &ofst); // ����� ��������������
		string shape_string_out(); // ����� �������������� � ������
		float shape_perim(); // ���������� ���������
	};

	// ���������, ������������ ����������
	class circle : public shape
	{
		int r; // ������ ����������
		int x, y; // ���������� ������ ����������
	public:
		circle(int t1, int t2, int t3, string t5, float t6) : r(t1), x(t2), y(t3), shape(t5, t6) {}; // ������������� ��������� � ������� ������������
		~circle(); // ����������
		void shape_add_data(ifstream &ifst); // ����
		void shape_out_data(ofstream &ofst); // �����
		string shape_string_out(); // ����� ���������� � ������
		float shape_perim(); // ���������� ���������
	};

	// ���������, ������������ �����������
	class triangle : public shape
	{
		int a, b, c; // ������� ������������
	public:
		triangle(int t1, int t2, int t3, string t5, float t6) : a(t1), b(t2), c(t3), shape(t5, t6) {}; // ������������� ��������� ������� ������������
		~triangle(); // ����������
		void shape_add_data(ifstream &ifst); // ����
		void shape_out_data(ofstream &ofst); // �����
		string shape_string_out(); // ����� ������������ � ������
		float shape_perim(); // ���������� ���������
	};

	// ���������, ����������� �� ������ ���������������� ��������� ������
	struct container
	{
		shape *form; // ������
		container *next; // ��������� ������� ����������
		container *prev; // ���������� ������� ����������
	};

	// �����, ����������� ���������
	class crisper
	{
		container *head, *tail; // ������ �� ������ � ��������� ��������
		int length; // ����� ����������
	public:
		crisper() : head(NULL), tail(NULL), length(0) {}; // ������������� ��������� � ���� ������� ������������
		~crisper() // ����������
		{
			cont_clear();
		};
		void cont_clear(); // �������� ����������
		void cont_sort(); // ���������� ��������� ����������
		void cont_add(ifstream &ifst); // ���� ��������� ����������
		void cont_out(ofstream &ofst); // ����� ��������� ����������
		void cont_rect_out(ofstream &ofst); // ����� ���������������, �������� � ���������
		int cont_len_out(); // ����� ����� ����������
		void cont_add_one(shape *this_one); // ���������� ������ �������� � ���������
		shape *get_shape(int i); // ����� ������ �� ����������
	};
};
#endif