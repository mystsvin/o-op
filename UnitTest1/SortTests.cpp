#include "stdafx.h"
#include "CppUnitTest.h"
#include "program.h"
#include <iostream>
#include <fstream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace all_shapes;
using namespace std;

namespace sort_Test
{
	TEST_CLASS(sortTests)
	{
	public:
		
		TEST_METHOD(main_Sort)
		{
			crisper Mine, Correct;
			shape *new_rectangle = new rectangle(1, 2, 3, 4, "Blue", 5);
			shape *new_circle = new circle(1, 2, 3, "Red", 8);
			shape *new_triangle = new triangle(1, 2, 3, "Yellow", 8);
			shape *another_circle = new circle(4, 2, 3, "Grey", 8);

			new_rectangle->shape_perim();
			new_circle->shape_perim();
			new_triangle->shape_perim();
			another_circle->shape_perim();

			Mine.cont_add_one(new_rectangle);
			Mine.cont_add_one(new_circle);
			Mine.cont_add_one(new_triangle);
			Mine.cont_add_one(another_circle);

			Correct.cont_add_one(new_triangle);
			Correct.cont_add_one(new_circle);
			Correct.cont_add_one(new_rectangle);
			Correct.cont_add_one(another_circle);

			Mine.cont_sort();

			Assert::AreEqual(Mine.cont_len_out(), Correct.cont_len_out());

			for (int i = 0; i < Mine.cont_len_out(); i++)
			{
				shape* First = Mine.get_shape(i);
				shape* Second = Correct.get_shape(i);
				Assert::AreEqual(First->shape_string_out(), Second->shape_string_out());
			}
		};
	};
}