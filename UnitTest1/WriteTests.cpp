#include "stdafx.h"
#include "CppUnitTest.h"
#include "program.h"
#include <fstream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace all_shapes;
using namespace std;


namespace write_Tests
{
	TEST_CLASS(WriteTests)
	{
	public:
		TEST_METHOD(write_Empty)
		{
			ofstream ofst("write_Empty.txt");
			crisper E;
			E.cont_out(ofst);

			ifstream ifst("write_Empty.txt");
			ifstream ifst_expect("write_ExpectedEmpty.txt");

			string result = "";
			string expectedResult = "";
			string s = "";

			while (getline(ifst, s)) result += s;
			while (getline(ifst_expect, s)) expectedResult += s;
			Assert::AreEqual(expectedResult, result);
		}
		TEST_METHOD(write_File)
		{
			ofstream ofst("write_Tests.txt");
			crisper Correct;

			shape *new_rectangle = new rectangle(1, 2, 3, 4, "Blue", 5);		
			shape *new_circle = new circle(1, 2, 3, "Red", 8);			
			shape *new_triangle = new triangle(1, 2, 3, "Yellow", 8);

			Correct.cont_add_one(new_rectangle);
			Correct.cont_add_one(new_circle);
			Correct.cont_add_one(new_triangle);
			Correct.cont_out(ofst);
			ofst.close();

			ifstream ifst("write_Tests.txt");
			ifstream ifst_expect("write_ExpectedFile.txt");

			string result = "";
			string expectedResult = "";
			string s = "";

			while (getline(ifst, s)) result += s;
			while (getline(ifst_expect, s)) expectedResult += s;
			Assert::AreEqual(expectedResult, result);
		}
	};
}