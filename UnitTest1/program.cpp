#include "stdafx.h"
#include <fstream>
#include "program.h"
#include "string"
#include "iostream"
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

namespace all_shapes
{
	// ����� ���������� � ����� ������, ��������� ������ � ��������� ���������
	string shape_info_out(string color, float density, float perim)
	{
		string out = ", color = " + color;
		out += ", density = " + to_string(density);
		out += ", perimeter = " + to_string(perim) + "\n";
		return out;
	}

	// ������ ��������� ��������������
	float rectangle::shape_perim()
	{
		return 2 * (abs(x1 - x2) + abs(y1 - y2));
	}

	// ���� ���������� ��������������
	void rectangle::shape_add_data(ifstream &ifst)
	{
		ifst.clear();
		if (!(ifst >> x1 >> y1 >> x2 >> y2))
			throw exception("Read Error: Incorrect Rectangle Data!");
	}

	// ����� ���������� ��������������
	void rectangle::shape_out_data(ofstream &ofst)
	{
		ofst << "It is Rectangle: x1 = " << to_string(x1) << ", y1 = " << to_string(y1) << ", x2 = " << to_string(x2) << ", y2 = " << to_string(y2);
		ofst << shape_info_out(color, density, shape_perim());
	}

	// ����� �������������� � ������
	string rectangle::shape_string_out()
	{
		string ans = "";
		ans += "It is Rectangle: x1 = " + to_string(x1) + ", y1 = " + to_string(y1) + ", x2 = " + to_string(x2) + ", y2 = " + to_string(y2);
		ans += shape_info_out(color, density, shape_perim());
		return ans;
	}

	// ����� ���������� ������ ��������������
	void rectangle::rectangle_out_data(ofstream &ofst)
	{
		shape_out_data(ofst);
	}

	// ������� ��������� ����������
	float circle::shape_perim()
	{
		return 2 * M_PI * r;
	}

	// ���� ���������� ����������
	void circle::shape_add_data(ifstream &ifst)
	{
		ifst.clear();
		if (!(ifst >> r >> x >> y))
			throw exception("Read Error: Incorrect Circle Data!");
		if (r < 1) throw exception("Read Error: Incorrect Circle Data!");
	}

	// ����� ���������� ����������
	void circle::shape_out_data(ofstream &ofst)
	{
		ofst << "It is Circle: r = " << to_string(r) << ", x = " << to_string(x) << ", y = " << to_string(y);
		ofst << shape_info_out(color, density, shape_perim());
	}

	// ����� ���������� � ������
	string circle::shape_string_out()
	{
		string ans = "";
		ans += "It is Circle: r = " + to_string(r) + ", x = " + to_string(x) + ", y = " + to_string(y);
		ans += shape_info_out(color, density, shape_perim());
		return ans;
	}

	// ������� ��������� ������������
	float triangle::shape_perim()
	{
		return a + b + c;
	}

	// ���� ���������� ������������
	void triangle::shape_add_data(ifstream &ifst)
	{
		ifst.clear();
		if (!(ifst >> a >> b >> c))
			throw exception("Read Error: Incorrect Triangle Data!");
		if ((a < 1) || (b < 1) || (c < 1)) throw exception("Read Error: Incorrect Triangle Data!");
	}

	// ����� ���������� ������������
	void triangle::shape_out_data(ofstream &ofst)
	{
		ofst << "It is Triangle: a = " << to_string(a) << ", b = " << to_string(b) << ", c = " << to_string(c);
		ofst << shape_info_out(color, density, shape_perim());
	}

	// ����� ������������ � ������
	string triangle::shape_string_out()
	{
		string ans = "";
		ans += "It is Triangle: a = " + to_string(a) + ", b = " + to_string(b) + ", c = " + to_string(c);
		ans += shape_info_out(color, density, shape_perim());
		return ans;
	}

	// ����� ��������� ������
	float shape::shape_perim_out()
	{
		return shape_perim();
	}

	// ���� ���������� ���������� ������
	shape* shape::shape_in(ifstream &ifst)
	{
		shape *sp;
		int k;

		ifst.clear();
		if (!(ifst >> k))
			throw exception("Read Error: Incorrect Key!");

		switch (k)
		{
		case 1:
			sp = new rectangle(0, 0, 0, 0, "", 0); // ������� ����������� ������� ��� ��������������
			break;
		case 2:
			sp = new circle(0, 0, 0, "", 0); // ������� ����������� ������� ��� ����������
			break;
		case 3:
			sp = new triangle(0, 0, 0, "", 0); // ������� ����������� ������� ��� ������������
			break;
		default:
			throw exception("Read Error: Incorrect Key!");
		}
		sp->shape_add_data(ifst);
		sp->perimeter = sp->shape_perim();

		ifst.clear();
		if (!(ifst >> sp->color))
			throw exception("Read Error: Incorrect Color!");

		ifst.clear();
		if (!(ifst >> sp->density))
			throw exception("Read Error: Incorrect Density!");
		if (sp->density < 1) throw exception("Read Error: Incorrect Density!");

		return sp;
	}

	// ����� ������ ������
	void shape::rectangle_out_data(ofstream &ofst)
	{
		ofst << endl;  // ������ ������
	}

	// ���������� ��� ������������ ������
	void crisper::cont_clear()
	{
		while (length != 0)
		{
			container *tmp = head;
			head = head->next;
			delete tmp;
			length--;
		}
	}

	// ���������� ���������� �� ���������
	void crisper::cont_sort()
	{
		container *tmp_head = head; // ��������� ������
		container *temp1, *temp2;

		temp1 = tmp_head;
		temp2 = tmp_head;

		for (int in1 = 0; in1 < length; in1++)
		{
			for (int in2 = 0; in2 < length; in2++)
			{
				if (temp1->form->shape_perim_out() < temp2->form->shape_perim_out())
				{
					shape *i = temp1->form;
					temp1->form = temp2->form;
					temp2->form = i;
				}
				temp2 = temp2->next;
				if (in2 + 1 == length)
					temp2 = tmp_head;
			}
			temp1 = temp1->next;
			if (in1 + 1 == length)
				temp1 = tmp_head;
		}
	}

	// ���������� �������� � ������
	void crisper::cont_add(ifstream &ifst)
	{
		if (!ifst)
			throw exception("Read Error: File don't Exist!");
		while (!ifst.eof()) {
			container *cont = new container;
			cont->form = shape::shape_in(ifst);
			if (cont->form != 0)
			{
				if (length == 0)
					head = tail = cont;
				else
				{
					tail->next = cont;
					cont->prev = tail;
					tail = cont;
				}

				length++;
			}
		}
	}

	// ����� ��������� � ����
	void crisper::cont_out(ofstream &ofst)
	{
		ofst << "Container contents " << length
			<< " elements." << endl;

		container *tmp_head = head;

		int temp = length;
		int i = 0;
		while (temp != 0)
		{
			ofst << i << ": ";
			i++;
			tmp_head->form->shape_out_data(ofst);
			tmp_head = tmp_head->next;
			temp--;
		}
	}

	// ����� ���������� ��������������
	void crisper::cont_rect_out(ofstream &ofst)
	{
		container *tmp_head = head;

		int temp = length;
		int i = 0;
		while (temp != 0)
		{
			i++;
			tmp_head->form->rectangle_out_data(ofst);
			tmp_head = tmp_head->next;
			temp--;
		}
	}

	// ����� ����� ����������
	int crisper::cont_len_out()
	{
		return length;
	}

	// ���������� ������ �������� � ���������
	void crisper::cont_add_one(shape *this_one)
	{
		container *cont = new container;
		cont->form = this_one;

		if (length == 0)
			head = tail = cont;
		else
		{
			tail->next = cont;
			cont->prev = tail;
			tail = cont;
		}

		length++;
	}

	// ����� ������ �� ����������
	shape* crisper::get_shape(int i)
	{
		container *tmp = head;

		int q = 0;
		while (i != q)
		{
			q++;
			tmp = tmp->next;
		}

		return tmp->form;
	}
}