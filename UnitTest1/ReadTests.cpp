#include "stdafx.h"
#include "CppUnitTest.h"
#include "program.h"
#include <iostream>
#include <fstream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace all_shapes;
using namespace std;

namespace read_Test
{
	TEST_CLASS(readTests)
	{
	public:

		TEST_METHOD(read_Empty)
		{
			ifstream ifst("nothing.txt");
			crisper E;

			bool check = false;
			try
			{
				E.cont_add(ifst);
			}
			catch (exception& exc)
			{
				string str = exc.what();
				if (str == "Read Error: Incorrect Key!")
					check = true;
			}
			Assert::AreEqual(true, check);
		};
		TEST_METHOD(read_Error)
		{
			ifstream ifst("wrong.txt");
			crisper Wrong;

			bool check = false;
			try
			{
				Wrong.cont_add(ifst);
			}
			catch (exception& exc)
			{
				string str = exc.what();
				if (str == "Read Error: Incorrect Density!")
					check = true;
			}
			Assert::AreEqual(true, check);
		};
		TEST_METHOD(read_Correct)
		{
			ifstream ifst("true.txt");
			crisper Wrong;
			Wrong.cont_add(ifst);

			crisper Correct;

			shape *new_rectangle = new rectangle(1, 2, 3, 4, "Blue", 5);
			Correct.cont_add_one(new_rectangle);
			shape *new_circle = new circle(1, 2, 3, "Red", 8);
			Correct.cont_add_one(new_circle);

			Assert::AreEqual(Correct.cont_len_out(), Wrong.cont_len_out());

			for (int i = 0; i < 2; i++)
			{
				shape* First = Correct.get_shape(i);
				shape* Second = Wrong.get_shape(i);
				Assert::AreEqual(First->shape_string_out(), Second->shape_string_out());
			}
		}
		TEST_METHOD(read_OneLine)
		{
			ifstream ifst("linear.txt");
			crisper Wrong;
			Wrong.cont_add(ifst);

			crisper Correct;

			shape *new_rectangle = new rectangle(1, 2, 3, 4, "Blue", 5);
			Correct.cont_add_one(new_rectangle);

			Assert::AreEqual(Correct.cont_len_out(), Wrong.cont_len_out());

			shape* First = Correct.get_shape(0);
			shape* Second = Wrong.get_shape(0);
			Assert::AreEqual(First->shape_string_out(), Second->shape_string_out());
		}
		TEST_METHOD(read_NotExist)
		{
			ifstream ifst("C:\\dontexist.txt");
			crisper Wrong;

			bool check = false;
			try
			{
				Wrong.cont_add(ifst);
			}
			catch (exception& exc)
			{
				string str = exc.what();
				if (str == "Read Error: File don't Exist!")
					check = true;
			}
			Assert::AreEqual(true, check);
		}
	};
}